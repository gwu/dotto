#!/bin/env bash

fcolor=#$(cat dot/col/moss | grep fore | cut -d# -f2)
bcolor=#$(cat dot/col/moss | grep back | cut -d# -f2)

color1=#$(cat dot/col/moss | grep lor1 | cut -d# -f2)
color2=#$(cat dot/col/moss | grep lor2 | cut -d# -f2)
color3=#$(cat dot/col/moss | grep lor3 | cut -d# -f2)

workspaces() {
	while read line; do
		if [ "$line" = true ]; then
			let activeworkspace--
			break # break if we found the active workspace 
		else
			activeworkspace="$line" # evaluate the next workspace
		fi
	done < <(i3-msg -t get_workspaces | jshon -a -e name -u -p -e focused -u)

	usedworkspacestotal=$(i3-msg -t get_workspaces \
		| jshon -a -e name -u \
		| tail -n 1)

	if [[ "$usedworkspaces" -lt 10 ]]; then
		usedworkspacestotal=10
	fi

	for ((i=0;i < $usedworkspacestotal;i++)) {
		if [[ $activeworkspace == $i ]]; then
			workspaceindicator+=('•')
		else
			workspaceindicator+=('·')
		fi
	}

	echo ${workspaceindicator[*]}
}

mpd() {
	if [[ $(mpc | wc -l) -gt 1 ]]; then
		echo $(mpc | head -1)
	fi
}

clock() {
	date +%H:%M
}

battery() {
	cat /sys/class/power_supply/BAT1/capacity
}

echo "" | lemonbar \
	-g 1x43+0+0 \
	-B "#00FFFFFF" \
	-p &

echo "" | lemonbar \
	-g 1886x32+17+10 \
	-B "$color2" \
	-d -p &

while true; do
	echo "%{B$bcolor}%{F$fcolor} \
		%{l}%{O10}$(workspaces) \
		%{c}$(mpd) \
		%{r}$(battery)% $(clock)%{O10}"
    # if [[ $(cat /sys/class/power_supply/BAT1/capacity) != 100 ]]; then
	    sleep 0.1 #cpu 70% because bash?
    # fi
done | lemonbar \
	-g 1882x28+19+12 \
	-f "scientifica:size=11:style=bold" \
	-f "VL Gothic:style=regular:size=6:antialias=false:hinting=false" \
	-F "#FFFFFF" \
	-B "#000000" \
	-d -p &
