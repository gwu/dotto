"	    __ _  _  _  __  _  _  ____   ___
"	   (  ( \/ )( \(  )( \/ )(  _ \ / __)
"	 _ /    /\ \/ / )( /    \ )   /( (__
"	(_)\_)__) \__/ (__)\_)(_/(__\_) \___)
"	dheu

source $DOTNVIMCONF/plug.vim
source $DOTNVIMCONF/plugconf.vim
source $DOTNVIMCONF/col.vim
source $DOTNVIMCONF/statline.vim
source $DOTNVIMCONF/map.vim
source $DOTNVIMCONF/aucmd.vim
source $DOTNVIMCONF/lig.vim

set clipboard+=unnamedplus
set pastetoggle=<F3>

set ignorecase

set tabstop=4
set shiftwidth=4
set noexpandtab
set nowrap
set list lcs=tab:\▏\ 

set encoding=UTF-8
scriptencoding UTF-8

set undodir=$HOME/.cache/nvim/undo undofile
set noshowmode

set nobackup nowritebackup noswapfile

set splitbelow splitright

set nonumber
set norelativenumber

set scl=yes
set nolist

" set laststatus=0

let g:python3_host_prog = "/usr/bin/python3"
