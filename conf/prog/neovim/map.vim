nnoremap <silent> gb	:bnext<CR>
nnoremap <silent> gB	:bprevious<CR>

nnoremap <silent> gm	:TagbarToggle<CR>:wincmd l<CR>

nnoremap <C-J>			<C-W><C-J>
nnoremap <C-K>			<C-W><C-K>
nnoremap <C-L>			<C-W><C-L>
nnoremap <C-H>			<C-W><C-H>

nnoremap Y				y$

nnoremap k				gk
vnoremap k				gk
nnoremap j				gj
vnoremap j				gj

function! Multiply()
	if v:count!=0
		let @r=v:count*expand('<cword>') | :norm diw"rP
	endif
endfunction

nnoremap <silent><C-m> :<C-u>call Multiply()<cr>

let mapleader ='<'
let maplocalleader = ';'

nnoremap <silent><leader>re :source /sai/dot/conf/prog/neovim/init.vim<CR>
nnoremap <silent><leader><space> :nohlsearch<CR>

nnoremap <leader>q :q<CR>
nnoremap <leader>Q :q!<CR>

vnoremap <silent><leader>ma yo<Esc>p^y$V:!perl -e '$x = <C-R>"; print $x'<CR>-y0j0P

nnoremap <silent><leader>c  :set scl=no<CR>
nnoremap <silent><leader>o  :set scl=yes<CR>

nnoremap <Tab>              za

" nnoremap <silent><leader>zf :FZF<CR>
" nnoremap <silent><leader>zb :Buffers<CR>

" nnoremap <silent><leader>mdo :MarkdownPreview<CR>
" nnoremap <silent><leader>mdc :MarkdownPreviewStop<CR>

" nnoremap <silent><leader>tf :NERDTreeToggle<CR>
" nnoremap <silent><leader>tu :UndotreeToggle<CR>:UndotreeFocus<CR>

" nnoremap <silent><leader>f :Goyo<CR><Esc>

" nnoremap <silent>			<C-n> :call comfortable_motion#flick(100)<CR>
" nnoremap <silent>			<C-p> :call comfortable_motion#flick(-100)<CR>
" vnoremap <silent>			<C-n> :call comfortable_motion#flick(100)<CR>
" vnoremap <silent>			<C-p> :call comfortable_motion#flick(-100)<CR>
