" dein configuration "
""""""""""""""""""""""

set runtimepath+=~/.local/share/nvim/dein/repos/github.com/Shougo/dein.vim

call dein#begin('~/.local/share/nvim/dein')


call dein#add('~/.local/share/nvim/dein')
" call dein#add('haya14busa/dein-command.vim')

call dein#add('Yggdroot/indentLine')

call dein#add('Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' })

call dein#add('vim-pandoc/vim-pandoc')
call dein#add('vim-pandoc/vim-pandoc-syntax')
call dein#add('jceb/vim-orgmode')
call dein#add('fatih/vim-go')
call dein#add('octol/vim-cpp-enhanced-highlight')
call dein#add('neovimhaskell/haskell-vim')
call dein#add('rust-lang/rust.vim')
call dein#add('racer-rust/vim-racer')
call dein#add('LnL7/vim-nix')
call dein#add('elzr/vim-json')
call dein#add('cespare/vim-toml')
call dein#add('davidbeckingsale/writegood.vim')

call dein#add('zchee/deoplete-go', {'build': 'make'})

call dein#add('Shougo/deoplete-clangx', {'depends': 'deoplete'})
call dein#add('eagletmt/neco-ghc', {'depends': 'deoplete'})

" call dein#add('prurigro/vim-polyglot-darkcloud',
            " \ {'depends': 'vim-go'},
            " \ {'depends': 'vim-json'})
call dein#add('baskerville/vim-sxhkdrc')

call dein#add('kana/vim-operator-user')
call dein#add('mattn/emmet-vim')
call dein#add('tpope/vim-speeddating')
call dein#add('tpope/vim-repeat')
call dein#add('machakann/vim-sandwich')
call dein#add('tpope/vim-commentary')
call dein#add('markonm/traces.vim')

" call dein#add('AndrewRadev/splitjoin.vim')
" call dein#add('wellle/targets.vim')

" call dein#add('aurieh/discord.nvim')

" call dein#add('ap/vim-css-color')
" call dein#add('sheerun/vim-polyglot')
" call dein#add('majutsushi/tagbar')
" call dein#add('junegunn/goyo.vim')

call dein#add('iamcco/mathjax-support-for-mkdp')
call dein#add('iamcco/markdown-preview.vim',
            \ {'depends': 'mathjax-support-for-mkdp'})

call dein#add('w0rp/ale')
" call dein#add('mhinz/vim-signify')
 
" call dein#add('scrooloose/nerdtree')
" call dein#add('Xuyuanp/nerdtree-git-plugin', {
"             \ 'depends': 'nerdtree' })
" call dein#add('mbbill/undotree')


" call dein#add('junegunn/fzf', {
            " \ 'path': '/home/dheu/.local/share/fzf',
            " \ 'build': './install --all',
            " \ 'merged': 0 })
" call dein#add('junegunn/fzf.vim', { 'depends': 'fzf' })

call dein#add('AndrewRadev/id3.vim')

call dein#end()
call dein#save_state()

packloadall
silent! helptags ALL
