" augroup NERDtree
"     autocmd!

"     autocmd StdinReadPre * let s:std_in=1
"     autocmd VimEnter * if
"                 \ argc() == 1 && isdirectory(argv()[0])
"                 \ && !exists("s:std_in")
"                 \ | exe 'NERDTree' argv()[0]
"                 \ | wincmd p
"                 \ | ene
"                 \ | endif
"     autocmd bufenter * if
"                 \ (winnr("$") == 1
"                 \ && exists("b:NERDTree")
"                 \ && b:NERDTree.isTabTree())
"                 \ | q
"                 \ | endif
" augroup END

augroup Term
    autocmd!
    
    au TermOpen * setlocal wrap
    au TermOpen * source $DOTNVIMCONF/col.vim
    au VimEnter,WinEnter * let &scrolloff = winheight(0) / 4
augroup END

augroup defaultFileTypes
    autocmd!

    autocmd BufRead,BufNewFile /tmp/calcurse* set filetype=pandoc
    autocmd BufRead,BufNewFile /sai/dot/conf/prog/calcurse/notes/* set filetype=pandoc
    autocmd BufNewFile,BufRead \*.{md,mdwn,mkd,mkdn,mark,ad\*} set filetype=pandoc
augroup END

function! s:insert_include_guards()
    let guardname = substitute(toupper(expand("%:t")), "\\.", "_", "g")
    execute "normal! i#ifndef " . guardname
    execute "normal! o#define " . guardname
    execute "normal! o"
    execute "normal! o"
    execute "normal! o"
    execute "normal! Go#endif /* " . guardname . " */"
    normal! kk
endfunction

augroup C
    autocmd!
    autocmd BufNewFile *.{h,hpp,hxx} call <SID>insert_include_guards()
augroup END
