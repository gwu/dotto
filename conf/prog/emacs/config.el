(use-package diminish
  :ensure t)

(use-package zerodark-theme
  :ensure t
  :init
  (load-theme 'zerodark t))

;;;(unless (package-installed-p 'spacemacs-theme)
;;;  (package-refresh-contents)
;;;  (package-install 'spacemacs-theme))

;;;(load-theme 'spacemacs-dark)
(require 'spaceline-config)
(spaceline-spacemacs-theme)
(setq powerline-default-separator 'wave)

(spaceline-compile)

(use-package fancy-battery
  :ensure t
  :init
  (fancy-battery-mode))

  (use-package beacon
    :ensure t
    :init
    (beacon-mode t))

  (use-package org-bullets
    :ensure t
    :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq ring-bell-function 'ignore)

(setq inhibit-startup-message t)
(global-hl-line-mode t)
(global-prettify-symbols-mode t)

(defun config-reload ()
  (interactive)
  (org-babel-load-file (expand-file-name "~/dot/conf/emacs.d/config.org")))
(global-set-key (kbd "C-x r e") 'config-reload)

(defalias 'yes-or-no-p 'y-or-n-p)
(setq scroll-conservatively 100)

(global-set-key (kbd "C-x C-b") 'ibuffer)
;;;  (setq ibuffer-expert t)

(use-package avy
  :ensure t
  :bind
  ("M-s" . avy-goto-char))

(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 2)
  (global-company-mode t))

(use-package company-irony
  :ensure t
  :config
  (require 'company)
  (add-to-list 'company-backends 'company-irony))

(use-package irony
  :ensure t
  :config
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'icony-cdb-autosetup-compile-options))

(with-eval-after-load 'company
  (add-hook 'c++-mode-hook 'company-mode)
  (add-hook 'c-mode-hook 'company-mode))



(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(setq ido-enable-flex-matching nil)
(setq ido-create-new-buffer 'always)
(setq ido-everywhere t)
(ido-mode 1)
(global-set-key (kbd "C-x b") 'ido-switch-buffer)

(use-package ido-vertical-mode
  :ensure t
  :init
  (ido-vertical-mode 1))
(setq ido-vertical-define-keys 'C-n-and-C-p-only)

(use-package smex
  :ensure t
  :init (smex-initialize)
  :bind
  ("M-x" . smex))

(defvar defterm "/bin/zsh")
(defadvice ansi-term (before force-bash)
  (interactive (list defterm)))
(ad-activate 'ansi-term)
(global-set-key (kbd "<s-return>") 'ansi-term)

(setq backup-inhibited t)
(setq auto-save-default nil)

(use-package which-key
  :ensure t
  :init
  (which-key-mode))
