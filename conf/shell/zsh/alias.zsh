alias sauce="source ${HOME}/.zshrc"

alias -s {conf,json,js,vim,sh,bash,zshc,c,h,py,html,css}="$EDITOR"
alias -s {mp3,m4a,flac}="ffplay -nodisp -hide_banner -autoexit"
         # {mp3,m4a,flac}="mpv --no-vid"
alias -s {png,jpg,tiff,bmp}="feh"
alias -s {mp4,mkv}="mpv"
