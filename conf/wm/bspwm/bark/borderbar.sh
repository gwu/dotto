#!/bin/bash

die() {
    pkill lemonbar
    exit 0
}

trap die SIGINT SIGKILL

BGROUND='#101716'
FGROUND='#30302F'

WM_NAME='bspwm'

BORDER='4'
WINGAP='10'
XOFFSET="$(( ${WINGAP}+${BORDER} ))"

HEIGHT='24'
WIDTH='1900'

BGDIM=${WIDTH}x${HEIGHT}+${WINGAP}+0

DIM=$(( ${WIDTH} - ${BORDER} - ${BORDER} ))x$(( ${HEIGHT} - ${BORDER} ))+${XOFFSET}+${YOFFSET}

BAR_FIFO=/tmp/bspwm_bar-socket

lemonbar -d -p -b -g "${BGDIM}" -B "${FGROUND}"
