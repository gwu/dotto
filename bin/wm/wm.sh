#!/usr/bin/sh

xrandr --output DP1 --auto --right-of eDP1
# xmodmap ${DOTSYSCONF}/X11/xmodmap
xset r rate 250 40
xinput set-prop 12 280 1

xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto

# dunst -conf $HOME/dot/wm/$1/dunstrc &
