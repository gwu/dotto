var fs = require('fs');
var http = require('http');

http.createServer(function(req, res) {
    fs.readFile('home.htm', function(err, page) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(page);
        res.end();
    });
}).listen(8080);
